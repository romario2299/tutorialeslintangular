# Instalación de ESLint y Prettier para proyectos TypeScript

## ESLint y Prettier en Angular

### 1. Implementación de ESLint

A pesar de que TSLint se encuentra en desuso, Angular continua usando este linter por defecto. Por lo tanto es importante realizar una migración de TSLint a ESLint.

En la raíz del proyecto usamos Angular CLI para añadir los siguientes paquetes:
```
ng add @angular-eslint/schematics
```
Ahora necesitamos migrarémos el proyecto con el siguiente comando:
```
ng g @angular-eslint/schematics:convert-tslint-to-eslint {{YOUR_PROJECT_NAME_GOES_HERE}}
```
Estos esquemas buscarán el archivo ```tslint.json``` e intentarán traducirlo en las reglas ESLint compatibles y las agregarán en un nuevo archivo llamado ```.eslintrc.json```. Modificará las configuraciones TSLint predeterminadas de Angular y usará las nuevas configuraciones de ESLint, y además reemplazará los comentarios o reglas deshabilitadas a lo largo del proyecto por su equivalente en ESLint. 

El archivo ```.eslintrc.json``` permite agregar reglas ya sea para archivos .ts o .html dentro del arreglo ```overrides```

La configuración de extends para archivos .ts debe quedar de la siguiente manera, con el fin de ampliar las reglas para TypeScript
```
"extends": [
        "plugin:@angular-eslint/recommended",
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:@angular-eslint/template/process-inline-templates"
      ],
```

### 2. Implementación de Prettier

Intalación de dependencias de prettier y prevención de conflictos con prettier
```
npm install -D prettier eslint-config-prettier eslint-plugin-prettier
```
Agregamos los complementos en el archivo ```.eslintrc.json```, en el apartado de .ts
```
"extends": [
        "plugin:@angular-eslint/recommended",
        "plugin:@angular-eslint/template/process-inline-templates",
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:prettier/recommended"
      ],
```
También podemos usar la configuración para archivos .html.
```
"extends": [
        "plugin:@angular-eslint/template/recommended",
        "plugin:prettier/recommended"
      ],
```

## Implementación de ESLint y Prettier en proyectos generales de TypeScript

Intalación de las dependencias de ESLint, el nuevo analizador de ESLint y reglas alternaticas para TypeScript respectivamente 
```
npm install --save-dev eslint
npm install --save-dev @typescript-eslint/parser
npm install --save-dev @typescript-eslint/eslint-plugin
```

Intalación de las dependencias de Prettier:
```
npm install --save-dev eslint-plugin-prettier 
npm install --save-dev prettier prettier-eslint eslint-config-prettier
```

Creación del archivo ```.eslintrc.json``` en la raíz del proyecto. Tendrá el esquema de configuración de ESLint:

```
{
  "parser": "@typescript-eslint/parser", // Specifies the ESLint parser
  "extends": [
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    "plugin:prettier/recommended" // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  "parserOptions": {
    "ecmaVersion": 2020, // Allows for the parsing of modern ECMAScript features
    "sourceType": "module" // Allows for the use of imports
  },
  "rules": {
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
  }
}
```

Creación del archivo ```.prettierrc.json``` para configurar o modificar las reglas establecidas de Prettier
```
{
  "singleQuote": true,
  "semi": true,
  "trailingComma": "none",
  "endOfLine": "auto",
  "printWidth": 15
}
```

Nota: 
1. Los archivos ```.eslintignore``` ```.prettierignore``` pueden ser creados en la raíz del proyecto para ignorar archivos o carpetas
2. La opción general de instalación de ESLint-TypeScript requiere agregar o modificar el comando del script en el ```package.json``` para linter por el siguiente:
```
eslint --fix \"src/**/*.ts\"
```
3. También es necesario agregar l comando del script para prettier:
```
prettier --write \"src/**/*.{ts,scss,html}\"
```

## Activar Prettier al guardar un archivo

Es posible guardar un archivo y que automáticamente se active el formateador de código Prettier. Para ello abrimos el archivo ```settings.json``` y agregamos ```"editor.formatOnSave": true``` para los tipos de archivos que queremos activar esta opción, es decir: JS, TS, html, etc. Ejemplo de un archivo ```settings.json```:
```
{
    "editor.minimap.enabled": false,
    "workbench.iconTheme": "material-icon-theme",
    "workbench.colorTheme": "Default Dark+",
    "editor.formatOnSave": false,
    "[typescript]": {
        "editor.defaultFormatter": "esbenp.prettier-vscode",
        "editor.formatOnSave": true
    },
    "workbench.sideBar.location": "left",
    "workbench.activityBar.visible": true,
    "terminal.integrated.shell.windows": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
    "[html]": {
        "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "editor.tabSize": 2,
    "editor.detectIndentation": false,
    "[javascript]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "[jsonc]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "[json]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "[scss]": {
      "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "angular.experimental-ivy": true
}
```